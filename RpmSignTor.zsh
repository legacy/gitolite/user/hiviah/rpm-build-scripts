#!/bin/zsh

setopt extendedglob


#if [ -z "$1" ]; then
#    echo "arg1 - dir with rpms"
#    exit 1
#fi


RSA=0xF4B85E0F

echo "New rpms:"
for I in tor-*.rpm; do
    echo " - $I"
done
echo ""

echo "Using new RSA key $RSA"
rpm --resign --define "_gpg_name  $RSA" tor-*.rpm



